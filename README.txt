-- SUMMARY --
Module dependency toggle is a small module that hides module
dependencies and provides a button for showing/hiding.
This module also works with module filter.


-- REQUIREMENTS --
JavaScript must be enabled for this module to work.


-- INSTALLATION --

  * Drupal 7
  Install as usual.
  See http://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


-- CONFIGURATION --
None required.
